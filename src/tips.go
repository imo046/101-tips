package src

import (
	"fmt"
	"io"
	"os"
	"reflect"
)

/*
*

	Do not use zero-sized field as last field in struct

*
*/
type MyStruct1 struct {
	id int
	_  [0]func()
}

//Needs more memory space than Struct1

type MyStruct2 struct {
	_  [0]func()
	id int
}

/**/

/*
*
Simulate i in 0..N
⋅*
*/
func RunCycle(n int) {
	const N = 8 //must be const
	for i := range [N]struct{}{} {
		fmt.Println(i)
	}
	fmt.Println("/")
	for i := range [N][0]int{} {
		fmt.Println(i)
	}
	fmt.Println("/")
	for i := range make([][0]int, N) {
		fmt.Println(i)
	}
}

/**/
/**
Ways to create a slice
⋅**/
func CreateSlice[A interface{}]() {
	var s0 = make([]A, 100)
	var s1 = (&[100]A{})[:]
	var s2 = new([100]A)[:]
	fmt.Println(reflect.TypeOf(s0), len(s0))
	fmt.Println(reflect.TypeOf(s1), len(s1))
	fmt.Println(reflect.TypeOf(s2), len(s2))
}

/**
for i,v = range aContainer actually iterates through a copy of aContainer (Array)
but for i, _ = range will not copy the container
In Go array owns elements but slice only refers to it, so for i, v will work on slices
*/

/*
*
Array pointers could be used as an arrays in some situations
*/
func IterateThroughArrayPointer() {
	a := [...]int{1: 10}
	på := &a
	//iterate through array
	for i, _ := range på {
		fmt.Println(på[i])
		_, _ = i, på[i]
	}

}

/**
Some functions are evaluated at compile time, such as unsafe.Sizeof, unsafe.Offsetof and unsafe.Alignof
*/
/**
Composite literals are unaddressable, but they may be taken addresses.
For example:
_ = &struct{ x int }
_ = &[]byte{}
_ = &map[int]bool{}
Composite literals are struct/array/slice/map
*/

/*
*
One liner to create pointers to non-zero bool/numeric/string values
*/
type MyConfig struct {
	OptionX *bool
	OptionY *bool
}

var x = true
var cfg = MyConfig{&x, &(&[1]bool{false})[0]} //three possible values: nil, &false, &true

/*
*
Use maps to emulate sets
*/
type Set map[int]struct{}

// map is already implemented as a pointer
func (s Set) Put(x int) {
	s[x] = struct{}{}
}
func (s Set) Has(x int) (res bool) {
	_, res = s[x] //second param is bool which maps return to check if element is present
	return
}
func (s Set) Remove(x int) {
	delete(s, x)
}

func (s Set) Get(k int) int {
	var i int
	for key, _ := range s {
		if i == k {
			return key
		}
		i++
	}
	return 0

}

func CreateSet() {
	s := make(Set)
	s.Put(2)
	s.Put(3)
	fmt.Println(len(s))
	fmt.Println(s)
	fmt.Println(s.Has(3))
	fmt.Println(s.Has(5))
	s.Remove(3)
	fmt.Println(s.Has(3))
	fmt.Println(s.Get(0))
}

func AppendSlice(s []int) []int {
	//passing slice to func will modify the existing slice, but using append won't be reflected in the original
	//When capacity or length are modified it will point to the new, bigger block of memory
	s = append(s, 10, 11, 12)
	return s
}

func ModifySlice(s []int) {
	s[0] = 0
}

// using slice as buffer while reading file
func ReadFile(filename string) ([]byte, error) {
	file, err := os.Open(filename)
	var fullData []byte
	if err != nil {
		return fullData, err
	}
	defer file.Close()
	data := make([]byte, 100)
	for {
		count, err := file.Read(data)
		fullData = append(fullData, data[:count]...)
		if err != nil && err != io.EOF {
			return fullData, err
		}
		if count == 0 {
			return fullData, nil
		}
	}
}
