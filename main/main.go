package main

import (
	"101-tips/src"
	"fmt"
)

func main() {

	src.CreateSet()
	s := []int{1, 2, 3, 4}
	src.ModifySlice(s)
	fmt.Println(s)
	src.AppendSlice(s)
	fmt.Println(s)

	res, err := src.ReadFile("")
	fmt.Println(err)
	fmt.Println(string(res))
}
